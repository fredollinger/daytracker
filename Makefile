APP=DayTracker
JAVAC=javac
JAVA=java
#JAVAC=/opt/java/bin/javac
#JAVA=/opt/java/bin/java
DESTPATH=com/fredollinger
JAVA_FILES = *.java

all:
	$(JAVAC) src/$(JAVA_FILES)
	mkdir -p $(DESTPATH)
	mv src/*.class $(DESTPATH)
	zenity --info

test:
	$(JAVA) -cp . $(DESTPATH)/$(APP)

clean:
	rm -rvf com
