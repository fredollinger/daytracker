package com.fredollinger;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.io.*;
import java.nio.file.Files;
import java.nio.charset.Charset;
import java.nio.file.Paths;

import java.util.Date;
import java.util.Calendar;

class DTDate {

    private Calendar now;
    private Date currentDate;

    String getResourceFile(String fileName) {
	String res="";
        int data;

        try {
	    InputStream inputStream = getClass().getResourceAsStream("/" + fileName);
	    Scanner s = new Scanner(inputStream).useDelimiter("\\A");
	    res = s.hasNext() ? s.next() : "";
	} // END try
        catch (Exception e) {
            System.out.println("Failed to open file: [" + fileName + "]");
            return res;
        }

        return res;
    } // END getResourceFile()

    void setDateFromFile(String file) {
	String rawdate = readFile(file);
	long epoch = Long.parseLong(rawdate.trim());
	currentDate = new Date(epoch);
    }

    long daysElapsed() {
	long later = now.getTime().getTime();
	long earlier = currentDate.getTime();
	return daysBetweenDates(later, earlier);
    }

    long secondsElapsed() {
	long later = now.getTime().getTime();
	long earlier = currentDate.getTime();
	return (later - earlier)/1000;
    }

    // Given two dates, calculate the number of days between them
    long daysBetweenDates(long later, long earlier) {
	return (later - earlier)/86400/1000;
    }
     
    // TODO: If the date file does not exist, make it with the current date
    // if it does exist, do nothing
    void initDateFile(String file) {
        File f = new File(file);
	if(f.exists()) { 
            System.out.println("File exists: [" + file + "]");
	    return;
        }

	long epoch = now.getTime().getTime();
	saveStringToFile(file, Long.toString(epoch));
    } // EMD initDateFile()

    void saveStringToFile(String file, String str) {
        BufferedWriter out = null;
        try  
        {
            FileWriter fstream = new FileWriter(file); //true tells to append data.
            out = new BufferedWriter(fstream);
            out.write(str);
        }
        catch (IOException e)
        {
            System.err.println("Error: " + e.getMessage());
        }
        finally
        {
            if(out != null) {
	        try {
                    out.close();
		}
                catch (IOException e)
                {
                    System.err.println("Error: " + e.getMessage());
                }
            }
        }
    } // END saveCurrentDate()

    static String readFile(String path) {
	String str="";
        try {	
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            str = new String(encoded, Charset.defaultCharset());
	}
        catch (IOException e)
        {
            System.err.println("Error: " + e.getMessage());
        }
	return str;
    }

    public DTDate() {
	now = Calendar.getInstance();
	currentDate = new Date();
    } // END DTDate()
} // END class DTDate
