package com.fredollinger;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.io.*;

import java.util.Date;
import java.util.Calendar;

import javax.swing.*;
import javax.swing.event.*;

import java.awt.*;
import java.awt.Event.*;

import javax.swing.text.*;
import javax.swing.text.html.*;

class DTWindow extends JFrame {
    String getFile(String fileName) {
	String res="";
        int data;

        try {
	    InputStream inputStream = getClass().getResourceAsStream("/" + fileName);
	    Scanner s = new Scanner(inputStream).useDelimiter("\\A");
	    res = s.hasNext() ? s.next() : "";
	} // END try
        catch (Exception e) {
            System.out.println("Failed to open file: [" + fileName + "]");
            return res;
        }

        return res;
    } // END getFile()

    // Return the saved date
    Date getDateFromFile(String file) {
	String rawdate = getFile(file);
	long epoch = Long.parseLong(rawdate.trim());
	Date d = new Date(epoch);
	return d;
    }

    // Given two dates, calculate the number of days between them
    // FIXME STUB
    long daysBetweenDates(long later, long earlier) {
	long diff = (later - earlier)/24/60/60;
	return diff;
    }

    public DTWindow() {
        JFrame frame = new JFrame( "Hello, Java" );
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JEditorPane p = new JEditorPane();
	Calendar now = Calendar.getInstance();
	long tlong = new Long(1489966921378L);
	Date tomorrow = new Date(tlong);
        p.setText(Long.toString(daysBetweenDates(tlong, getDateFromFile("curdate.html").getTime()))); // Document text is provided below.
        //p.setText(Long.toString(epoch)); // Document text is provided below.
	//long epoch = Long.parseLong(now.getTime().getTime());
	//long epoch = now.getTime().getTime();
        //p.setText(Long.toString(epoch)); // Document text is provided below.
        //p.setText(now.getTime().toString()); // Document text is provided below.
        //p.setText(getDateFromFile().toString()); // Document text is provided below.
        //p.setText(tomorrow).toString(); // Document text is provided below.
	//String html = getFile("index.html");
        // p.setText(d.toString()); // Document text is provided below.
        frame.add(p);
        frame.setSize( 600, 400 );
        frame.setVisible( true );
    } // END DTWindow()
} // END class RagWindow
