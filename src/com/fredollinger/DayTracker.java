package com.fredollinger;
import javax.swing.*;
import java.nio.charset.Charset;

// package fredollinger;

public class DayTracker {
    public static void main ( String[] args ){
        // DTWindow win = new DTWindow(); // use this to test window code
	DTDate dtd = new DTDate();
	String file = "datefile.txt";
        dtd.initDateFile(file);
	//String content = dtd.readFile("datefile.txt");
        dtd.setDateFromFile(file);
        System.out.println("days elapsed: [" + dtd.daysElapsed() + "]");
        //System.out.println("file: [" + content + "]");
	// dtd.setDateFromFile(file);
	// Date d = dtd.getDateFromFile("datefile.txt");
        // dtd.saveStringToFile("fredfile", "replace string"); 
    } // END main
} // END class DayTracker
